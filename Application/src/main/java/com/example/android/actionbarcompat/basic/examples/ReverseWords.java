package com.example.android.actionbarcompat.basic.examples;

/**
 * Reverse the order of words in a string.
 */
public class ReverseWords {

    public String reverse(String line) {
        // Detects where words start and begin, and calls reverse().
        int word_start = 0, word_end, in_word = 0, count;
        int lim = line.length();
        char curr;
        StringBuilder sb = new StringBuilder(line);

        // Reverse individual words.
        for(count = 0; count < lim; count++) {
            curr = sb.charAt(count);

            if(in_word == 0 && curr != ' ') {
                in_word = 1;
                word_start = count;
            }
            else if(in_word > 0 && curr == ' ') {
                in_word = 0;
                word_end = count - 1;
                reverseWord(word_start, word_end, sb);
            }
            else if(count == lim - 1)
                reverseWord(word_start, count, sb);
        }

        // Reverse entire string.
        reverseWord(0, lim-1, sb);

        return sb.toString();
    }

    private void reverseWord(int lower, int upper, StringBuilder line) {
        // Reverses the chars in a string.
        int lim = (upper + lower) / 2;
        char swap;

        for(lower = lower; lower <= lim; lower++) {
            swap = line.charAt(lower);
            line.setCharAt(lower, line.charAt(upper));
            line.setCharAt(upper, swap);
            upper--;
        }
    }
}
